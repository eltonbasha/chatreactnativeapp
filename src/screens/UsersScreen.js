import React from 'react';
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView, Button, FlatList } from 'react-native';
import * as Styles from '../config/styles';
import * as firebase from 'firebase';
import * as Utils from '../Utils';
import { TouchableListItem } from '../utils/UtilComponents';

export class UsersScreen extends React.Component {
    constructor(props) {
        super(props);
        this.onlineUsers = [];
        this.state = {
            isRefreshin: false
        }
        this.handleClick = this.handleClick.bind(this);
        this.subscribeToDatabase();
    }

    subscribeToDatabase() {
        Utils.readUser()
            .then((userId) => {
                firebase.database().ref('/user_status')
                    .on('value', this.addNewData(userId));
            })
            .catch((error) => {
                console.log(error);
            });
    }

    unsubscribeFromDatabase() {
        firebase.database().ref('/user_status')
            .off();
    }

    _renderUser() {
        // this.state.isRefreshin = false;
        return (user) => {
            let userName = user.item.username;
            let onlineStatus = user.item.status;
            let userId = user.item.uid;
            return (
                <TouchableListItem
                    id={userId}
                    uname = {userName}
                    key={userId}
                    onPressItem={this.handleClick}
                    title={userName}
                    subtitle={onlineStatus ? "Online" : "Offline"} />
            )
        };
    }

    componentWillUnmount() {
        console.log("Unsubscribing from database");
        this.unsubscribeFromDatabase();
    }

    render() {
        return (
            <View >
                <FlatList
                    data={this.onlineUsers}
                    extraData={this.state.isRefreshin}
                    renderItem={this._renderUser()}
                    refreshing={this.state.isRefreshin} />
            </View>
        );
    }

    sortUsers() {
        return ((user1, user2) => {
            console.log("User ne sort: " + JSON.stringify(user1))
            let status1 = user1.status ? 1 : 0;
            let status2 = user2.status ? 1 : 0;
            return status2 - status1;
        })
    }

    addUsers(newUser) {
        this.onlineUsers.map((user) => {
            user.uid == newUser.uid ? user.status = newUser.status : "";
        })
    }

    addNewData(myUid) {
        return ((snapshot) => {
            this.onlineUsers = [];
            let snapJSOn = JSON.stringify(snapshot);
            let itemSnapshot = JSON.parse(snapJSOn);
            console.log("itemSnapShot", itemSnapshot);
            Object.entries(itemSnapshot).forEach((child) => {
                console.log("CHILDDD :" + JSON.stringify(child));
                let contact = child[1];
                contact["uid"] = child[0];
                console.log("Child i snapshotit", contact);
                contact.uid != myUid ? this.onlineUsers.push(contact) : "";
            });
            this.onlineUsers.sort(this.sortUsers());
            console.log("SNAPSHOOT" + JSON.stringify(snapshot));
            this.setState({
                isRefreshin: true
            })
        })
    }

    handleClick(id,uname) {
        this.props.navigation.navigate('ChatRoom' , {userId : id,username: uname});

        console.log("Id ne click: " + id);
    }
}