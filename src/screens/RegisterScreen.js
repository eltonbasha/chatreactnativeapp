import React from 'react';
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView, ScrollView } from 'react-native';
import { FormLabel, FormInput, Input, Button } from 'react-native-elements'
import * as Styles from '../config/styles';
import * as Utils from '../Utils';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import * as FirebaseDb from '../data/AuthDBAccess';
const md5 = require('blueimp-md5');

export class RegisterScreen extends React.Component {
    constructor(props) {
        super(props);

        let field = function () {
            this._value = null;
            this.set = function (v) {
                this._value = v;
            };
            this.value = function () {
                return this._value;
            };
            this.length = function () {
                return this._value ? this._value.length : 0
            }
        };

        this.state = {
            name: new field(),
            surname: new field(),
            username: new field(),
            password: new field(),
            email: new field()
        };
    }

    render() {
        return (
            <View style={Styles.styles.outer}>
                <ScrollView>
                    <FormLabel>Username</FormLabel>
                    <FormInput
                        onChangeText={this.checkFieldIsUnique(this.state, this.state.username)} />

                    <FormLabel>Name</FormLabel>
                    <FormInput
                        onChangeText={this.checkFieldIsUnique(this.state, this.state.name)} />

                    <FormLabel>Surname</FormLabel>
                    <FormInput
                        onChangeText={this.checkFieldIsUnique(this.state, this.state.surname)} />

                    <FormLabel>Email</FormLabel>
                    <FormInput
                        onChangeText={this.checkFieldIsUnique(this.state, this.state.email)} />

                    <FormLabel>Password</FormLabel>
                    <FormInput
                        secureTextEntry={true}
                        onChangeText={this.checkFieldIsUnique(this.state, this.state.password)} />
                    <Button
                        large
                        raised
                        title="Register"
                        onPress={() => { return this.registerClick() }}
                    />
                </ScrollView>
                <KeyboardSpacer />
            </View>
        );
    }

    checkFieldIsUnique(state, field) {
        return function (value) {
            let isUnique = Object.values(state)
                .every(
                    (field) => { return value != field.value() }
                );
            isUnique ? field.set(value) : field.set(null);
        };
    }

    registerClick() {
        if (this.checkDataEntry()) {
            FirebaseDb.checkUsernameExists(this.state.username.value())
                .then((exists) => {
                    console.log("Exitss : " + exists);
                    exists ?
                        alert("This username already exists") :
                        FirebaseDb.registerUser(
                            {
                                email: this.state.email.value(),
                                name: this.state.name.value(),
                                password: this.state.password.value(),
                                surname: this.state.surname.value(),
                                username: this.state.username.value()
                            })
                            .then((ref) => {
                                let userId = ref.key;
                                return FirebaseDb.updateUserStatus(userId, this.state.username.value());
                            })
                            .then(() => {
                                this.props.navigation.navigate('AuthLoading');
                            })
                            .catch((error) => {
                                console.log(error);
                            });
                })
        }
    }

    checkDataEntry() {
        let passwordValidity = this.validatePassword();
        let checkFields = Object.values(this.state)
            .every((text) => {
                console.log(text.length())
                return text.length() != 0
            });
        if (!checkFields) {
            alert("Please fill in all fields!")
            return false;
        } else {
            return passwordValidity && checkFields && this.validateEmail();
        }
    }

    /**Validate the password
     * if it contain+s a lowercase letter,
     * an uppercase letter,contains digits 0-9
     * and is at least 6 chars long
     */
    validatePassword() {
        console.log("password: " + this.state.password.length());
        let passwordValidity = this.state.password.value() ? this.state.password.value().match(/((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})/) : false;
        if (passwordValidity) {
            return true;
        } else {
            alert("Make sure your password contains at least one capital letter and a number!")
            return false;
        }
    }

    insertEntry(text, stateField) {
        this.checkEntry(text) ? this.state[stateField] = text : this.state[stateField] = ""
    }

    validateEmail() {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let emailValidity = this.state.email.value().toLowerCase().match(re);
        if (emailValidity) {
            return true;
        } else {
            alert("The email must be a valid email!")
            return false;
        }
    }
}