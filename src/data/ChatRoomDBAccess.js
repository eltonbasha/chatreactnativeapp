import * as firebase from 'firebase';
import * as Utils from '../Utils';

export function createNewChatMessage(data, text) {
    let firebaseDb = firebase.database();
    let chatMessagesNewNodeId = firebaseDb.ref('/chat_messages').child(data.chatRoomId)
    let chatMessageNewAutoGenerateId = chatMessagesNewNodeId.push();
    data["messageNewId"] = chatMessageNewAutoGenerateId.key;
    let currentTimestamp = Date.now();
    return (chatMessageNewAutoGenerateId.set({
        message: text,
        sent_by: data.myId,
        timestamp: currentTimestamp
    }).then(() => {
        data.chatExists ? console.log('Message sent') : ""
        return data;
    }));
}

export function createNewChatRoomRefForUser(data, username) {
    let firebaseDb = firebase.database();
    let usersRef = '/users/' + data.myId + '/rooms/' + data.userId + '/';
    let otherUser = '/users/' + data.userId + '/rooms/' + data.myId + '/';
    let data1 = {};
    data1[usersRef] = {
        [data.chatRoomId]: true,
        username: username
    };
    return Utils.readUsername()
        .then((myUsername) => {
            data1[otherUser] = {
                [data.chatRoomId]: true,
                username: myUsername
            };
            return firebaseDb
                .ref()
                .update(data1)
                .then((snapshot) => {
                    return data;
                })
        })
}

export function createChatRoomInDB(myId, userId) {
    let firebaseDb = firebase.database();
    let chatRoomNewId = firebaseDb.ref('/chat_room')
        .push();
    this.chatId = chatRoomNewId.key;
    let chatRoomObject = {};
    chatRoomObject[myId] = true;
    chatRoomObject[userId] = true;
    return chatRoomNewId.set({
        room_name: "",
        users: chatRoomObject
    })
        .then(() => {
            return { myId: myId, userId: userId, chatRoomId: chatRoomNewId.key };
        });
}


export function getChatRoom(userId, myId) {
    let path = '/users/' + myId + '/rooms/' + userId;
    return firebase.database()
        .ref(path);
    //.once('value');
}

/**************************FILE AND IMAGE UPLOADER**************************** */
export const uploadImage = (uri, chatId, userId, name) => {
    let imagesPath = chatId + '/images'
    let imageName = name ? name : Date.now() + ''; //Checks if it's a file or an image(images don't have a name)
    const imageRef = firebase.storage().ref(imagesPath).child(imageName);
    let response = fetch(uri)
        .then((response) => {
            return response.blob()
        })
        .then((blob) => {
            return imageRef.put(blob);
        })
        .then((snapshot) => {
            return snapshot.downloadURL;
        })
        .then((downloadURL) => {
            storeReference({ chatRoomId: chatId, myId: userId, url_image: downloadURL })
        })
        .catch((error) => {
            console.log(error);
        });
};

async function storeReference(data) {
    let firebaseDb = firebase.database();
    let reference = '/chat_messages/' + data.chatRoomId;
    let chatMessagesNewNodeId = firebaseDb.ref('/chat_messages').child(data.chatRoomId)
    let chatMessageNewAutoGenerateId = chatMessagesNewNodeId.push();
    data["messageNewId"] = chatMessageNewAutoGenerateId.key;
    let currentTimestamp = Date.now();
    console.log("Url download : " + data.url_image);
    return (chatMessageNewAutoGenerateId.set({
        message: "Image",
        sent_by: data.myId,
        timestamp: currentTimestamp,
        url: data.url_image
    }, (snapshot) => {
        // console.log("Message id:" + snapshot.key)
    }));
}
