import React from 'react';
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView } from 'react-native';
import { FormLabel, FormInput, Input, Button } from 'react-native-elements'
import * as Styles from '../config/styles';
import * as firebase from 'firebase';
import * as Utils from '../Utils';
import { TabNavigator } from 'react-navigation';


export class SettingsScreen extends React.Component {
    render() {
        return (
            <View style = {Styles.defaultStyles.container}>
                <Button
                  containerViewStyle={{marginLeft:-10,marginRight:null,width:"100%"}}
                  buttonStyle={Styles.styles.loginButton}
                large
                raised
                title='Log Out'
                onPress={() => { return this.logOut()}}/>
            </View>
        );
    };

    logOut(){
        Utils.readUser()
        .then((userId) => {
            firebase.database()
            .ref('/user_status/' + userId + '/status')
            .set(false);
        })
        .then(() => {
            Utils.clearUser();
        })
        .then(() => {
            this.props.navigation.navigate('AuthLoading');
        })
        .catch((error) => {
            console.log(error);
        });
        
    }
}