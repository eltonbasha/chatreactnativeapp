import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {StackNavigator, SwitchNavigator , TabNavigator } from 'react-navigation';
import * as Config from './src/config/config';
import * as firebase from 'firebase';
import { LoadingScreen } from './src/screens/LoadingScreen';

firebase.initializeApp(Config.firebaseConfig);


//import { LoadingScreen } from './src/screens/LoadingScreen';


/**
 * The navigator will be used as entry point to the app since it returns
 * a React Component and it will handle which screen to show as home
 */

const SwitchNav = SwitchNavigator({
        App: Config.AppStack,
        Auth: Config.LoginStack,
        AuthLoading: LoadingScreen,
    }
    ,
    {
        initialRouteName: 'AuthLoading'
    });

export default class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SwitchNav/>
        );
    }
};