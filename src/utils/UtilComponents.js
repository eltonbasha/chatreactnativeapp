import React from 'react';
import { View, Text, TouchableHighlight, Image, Linking } from 'react-native';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput'
import { ListItem } from 'react-native-elements'
import * as Styles from '../config/styles';

export class InputBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentText: ""
        }
    }

    _onSendPressed = () => {
        this.setState({
            currentText: ""
        });
        this.props.onSendPressed(this.state.currentText);
    }

    _onSendImagePressed = () => {
        this.props.onSendImagePressed();
    }

    _onSendFilePressed = () => {
        this.props.onSendFilePressed();
    }

    render() {
        return (
            <View style={Styles.styles.inputBar}>
                <TouchableHighlight style={Styles.styles.fileButton}
                    onPress={this._onSendFilePressed}
                >
                    <Text style={{ color: 'white' }}>File</Text>
                </TouchableHighlight>
                <TouchableHighlight style={Styles.styles.fileButton}
                    onPress={this._onSendImagePressed}
                >
                    <Text style={{ color: 'white' }}>Img</Text>
                </TouchableHighlight>
                <AutoGrowingTextInput style={Styles.styles.textBox}
                    multiline={true}
                    defaultHeight={100}
                    onChangeText={(text) => { this.setState({ currentText: text }) }}
                    onContentSizeChange={this.props.onSizeChange}
                    value={this.state.currentText} />
                <TouchableHighlight style={Styles.styles.sendButton}
                    onPress={this._onSendPressed}
                >
                    <Text style={{ color: 'white' }}>Send</Text>
                </TouchableHighlight>

            </View>
        );
    }
}


export class MessageBubble extends React.Component {
    render() {
        console.log("URLLL :" + this.props.storageUrl + " direction : " + this.props.direction)
        //These spacers make the message bubble stay to the left or the right, depending on who is speaking, even if the message is multiple lines.
        var leftSpacer = this.props.direction === 'left' ? null : <View style={{ width: 70 }} />;
        var rightSpacer = this.props.direction === 'left' ? <View style={{ width: 70 }} /> : null;

        var bubbleStyles = this.props.direction === 'left' ? [Styles.styles.messageBubble, Styles.styles.messageBubbleLeft] : [Styles.styles.messageBubble, Styles.styles.messageBubbleRight];

        var bubbleTextStyle = this.props.direction === 'left' ? Styles.styles.messageBubbleTextLeft : Styles.styles.messageBubbleTextRight;


        return (
            this.props.storageUrl ? <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                {leftSpacer}
                <View style={bubbleStyles}>
                    <ImageWithDefault
                        style={bubbleStyles}
                        resizeMode='center'
                        default={this.props.storageUrl} >
                    </ImageWithDefault>
                </View>
                {rightSpacer}
            </View>
                :
                <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    {leftSpacer}
                    <View style={bubbleStyles}>
                        <Text style={bubbleTextStyle}>
                            {this.props.text}
                        </Text>
                    </View>
                    {rightSpacer}
                </View>
        );
    }
}

export default class ImageWithDefault extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            failed: false
        };
    }

    _onClick = () => {
        Linking.openURL(this.props.default);
    }

    _onError = () => {
        this.setState({ failed: true });
    }

    render() {
        const defaultImage =
            <Text style={this.props.style} onPress={this._onClick}>
                Open File/Image {this.props.key}</Text>
            ;

        if (this.state.failed) return defaultImage;

        return (
            <Text style={this.props.style} onPress={this._onClick}>
                Open File/Image {this.props.key}</Text>
        );
    }
}

export class TouchableListItem extends React.Component {
    _onPress = () => {
        this.props.onPressItem(this.props.id, this.props.uname);
    };

    render() {
        console.log("Props : " + JSON.stringify(this.props));
        const { children } = this.props;
        return (
            <ListItem onPress={this._onPress}
                {...this.props}>
            </ListItem>
        );
    }
}
