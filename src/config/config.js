/**

 * Config file containing all configurations like

 * Firebase Config and react-navigation config

 */


import { LoginScreen } from '../screens/LoginScreen';

import { RegisterScreen } from '../screens/RegisterScreen';
import { Platform, StatusBar } from 'react-native';
import { UsersScreen } from '../screens/UsersScreen';
import { SettingsScreen } from '../screens/SettingsScreen';
import { ChatListScreen } from '../screens/ChatListScreen';
import { ChatRoom } from '../screens/ChatRoomScreen';
import { StackNavigator, TabNavigator } from 'react-navigation';


/**

 * Constant which contains the navigator config object

 * in which the screens are configured.

 */


const navigatorOptions = {

    initialRoute: 'Login',

    headerTintColor: 'black',

    title: "Chat Hello",

    navigationOptions: {

        height: 50,

        headerStyle: {
            backgroundColor: '#00897b',
            height: 50,
        },

        headerTitleStyle: {

            fontSize: 15,

            fontWeight: 'bold',

        }

    },



};


const tabBarConfiguration = {

    tabBarOptions: {

        activeTintColor: 'white',

        labelStyle: {

            fontSize: 15,



        },

        style: {

            backgroundColor: '#00897b',

            borderTopWidth: 0,

            height: 70,

            justifyContent: "flex-end"

        },

    }

};

const authNavigatorConfig = {


    Login: { //Home eshte emri i route

        screen: LoginScreen,

        title: "Login",

    },

    Register: {

        screen: RegisterScreen,

        title: 'Register'

    }

}


export const LoginStack = TabNavigator(authNavigatorConfig, tabBarConfiguration);


const appNavigatorConfig = {

    Chats: {

        screen: ChatListScreen,

        title: "Chats"

    },

    Users: {

        screen: UsersScreen,

        title: "Users",

    },

    Settings: {

        screen: SettingsScreen,

        title: "Settings"

    }

}


export const MenuNavigator = TabNavigator(appNavigatorConfig, tabBarConfiguration);




const appStack = {

    TabStack: {

        screen: MenuNavigator,

        title: "Home",

    },

    ChatRoom: {

        screen: ChatRoom,

        title: "Home"

    }


}

export const AppStack = StackNavigator(appStack, navigatorOptions);


export const firebaseConfig = {

    apiKey: "AIzaSyAUnTW-CuOkl91si0bUL2ikemXdXw8PJ10",

    authDomain: "fir-test-d043d.firebaseapp.com",

    databaseURL: "https://fir-test-d043d.firebaseio.com",

    projectId: "fir-test-d043d",

    storageBucket: "fir-test-d043d.appspot.com",

    messagingSenderId: "431049202296"

};
