import React from 'react';
import { View, FlatList, Text, TouchableHighlight, Keyboard } from 'react-native';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput'
import KeyboardSpacer from 'react-native-keyboard-spacer'
import * as Styles from '../config/styles';
import * as firebase from 'firebase';
import * as Utils from '../Utils';
import { InputBar, MessageBubble } from '../utils/UtilComponents';
import { ImagePicker, DocumentPicker } from 'expo';
import * as FirebaseDb from '../data/ChatRoomDBAccess';

export class ChatRoom extends React.Component {

    constructor(props) {
        super(props);
        this.messages = [];
        this.state = {
            isRefreshing: false,
            first_chat: false
        }
        this._sendMessage = this._sendMessage.bind(this);
        const { params } = this.props.navigation.state;
        this.userId = params ? params.userId : null;
        this.chatId = params ? params.chatId : null;
        this.myId = ""
        this.lastMessageId = "";
        this.firstMessageId = "";
        this.scrollView;
        this._init();
    }

    render() {
        const { params } = this.props.navigation.state;
        return (
            <View style={Styles.styles.outer}>
                <FlatList
                    ref={(ref) => { this.scrollView = ref }}
                    data={this.messages}
                    extraData={this.state.messages}
                    renderItem={this._renderMessage()}
                    refreshing={this.state.isRefreshing}
                     />
                <InputBar
                    onSendPressed={this._sendMessage}
                    onChangeText={(text) => { console.log(text) }}
                    onSendImagePressed={this.openImagePicker}
                    onSendFilePressed={this.openFilePicker}
                />
                <KeyboardSpacer />
            </View>
        );
    }

    openImagePicker = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
        })
            .then((response) => {
                console.log("response i image picker " + JSON.stringify(response.cancelled));
                if (!response.cancelled) {
                    FirebaseDb.uploadImage(response.uri, this.chatId, this.myId);
                }
                return response;
            })
            .catch((error) => {
                console.log("Line 64 : " + error);
            });
    }

    openFilePicker = async () => {
        let result = await DocumentPicker.getDocumentAsync()
            .then((response) => {
                console.log("response i image picker " + JSON.stringify(response.cancelled));
                if (!response.cancelled) {
                    FirebaseDb.uploadImage(response.uri, this.chatId, this.myId, response.name);
                }
                return response;
            })
            .catch((error) => {
                console.log("Line 78 : " + error);
            });
    }

    _sendMessage = (text) => {
        console.log("text ktu : " + text);
        if (text.length != 0) {
            this.chatId ? this.sendMessageToDb(text) : this.createChat(text)
        }
    }

    createChat(text) {
        const { params } = this.props.navigation.state;
        const userId = params ? params.userId : null;
        let username = params ? params.username : null;
        console.log("hello username : " + username);
        Utils.readUser()
            .then((myId) => {
                return FirebaseDb.createChatRoomInDB(myId, userId);
            })
            .then((data) => {
                this.chatId = data.chatRoomId;
                this._subscribeTest(data.chatRoomId)
                return FirebaseDb.createNewChatMessage(data, text)
            })
            .then((data) => {
                return FirebaseDb.createNewChatRoomRefForUser(data, username)
            })
            .then((data) => {
                this.setState({
                    isRefreshing: true
                });
            })
            .catch((error) => {
                console.log("Line 112 : " + error);
            });
    }

    sendMessageToDb(text) {
        Utils.readUser()
            .then((userId) => {
                data = {
                    myId: userId,
                    chatRoomId: this.chatId,
                    chatExists: true
                }
                console.log("chat Id " + JSON.stringify(data));
                FirebaseDb.createNewChatMessage(data, text);
            })
    }

    _renderMessage() {
        return (user) => {
            const { params } = this.props.navigation.state;
            let userId = params ? params.userId : null;
            let direction = userId == user.item.sent_by ? "left" : "right";
            return (
                <MessageBubble
                    storageUrl={user.item.url}
                    direction={direction}
                    key={user.item.timestamp}
                    text={user.item.message} />
            )
        }
    }

    getRoomMessages(roomId) {
        this.chatId = roomId;
        let path = '/chat_messages/' + roomId;
        return firebase.database()
            .ref(path)
            .orderByKey()
            .limitToLast(15)
            .once('value', (snapshoot) => {
                console.log("Snapshoot i messageve : " + JSON.stringify(snapshoot));
                this.chatId = snapshoot.key;
                this.messages = Utils.snapshotToArray(snapshoot);
                this.lastMessageId = this.messages[this.messages.length - 1].key;
                this.firstMessageId = this.messages[0].key;
                this.setState({
                    isRefreshing: true
                })
            })
            .then(() => {
                this._subscribeTest(roomId);
            });
    }

    _prependMessage() {
        console.log("text ktu prepend: ");
        if (this.firstMessageId.length != 0) {

            let path = '/chat_messages/' + this.chatId;
            return firebase.database()
                .ref(path)
                .orderByKey()
                .limitToLast(5)
                .endAt(this.firstMessageId)
                .once('value', (snapshoot) => {
                    console.log("snapshoot p: " + JSON.stringify(snapshoot));
                    var firstMessageIdBefore = this.firstMessageId;
                    var messages = this.messages;
                    var test = [];
                    var change = true;
                    snapshoot.forEach(function (childSnapshot) {
                        if (test.length != childSnapshot.length - 1) {
                            var item = childSnapshot.val();
                            item.key = childSnapshot.key;
                            test.unshift(item);
                            if (change) {
                                firstMessageIdBefore = childSnapshot.key;
                            }
                            change = false;
                        }
                    });
                    test.forEach(function (testItem) {
                        messages.unshift(testItem);
                    });
                    this.firstMessageId = firstMessageIdBefore;
                    console.log("firstMessageId" + this.firstMessageId);
                    this.setState = {
                        isRefreshing: true
                    }
                });
        }
    }

    _init() {
        console.log("UserId : " + this.userId);
        Utils.readUser()
            .then((myId) => {
                console.log("My Id : " + myId);
                this.myId = myId;
                let path = '/users/' + myId + '/rooms/' + this.userId;
                console.log("helllo patj " + path);
                firebase.database()
                    .ref(path)
                    .once("value", function (snapshoot) {
                        if (snapshoot.val() != null) {
                            Object.keys(snapshoot.val())
                                .forEach((item) => {
                                    if (item != 'username') {
                                        let roomdId = item;
                                        console.log("room id :" + roomdId);
                                        this.roomdId = roomdId;
                                        //this._subscribeTest(roomdId);
                                        return this.getRoomMessages(roomdId);
                                    }
                                })
                        }
                    }.bind(this));
            })
            .catch((error) => {
                console.log("Line 192 : "+error);
            });
    }

    unsubscribeFromDatabase() {
        let path = '/users/' + this.myId + '/rooms/' + this.userId;
        firebase.database().ref(path).off();
    }

    getChatRoom(userId, myId) {
        let path = '/users/' + myId + '/rooms/' + userId;
        return firebase.database()
            .ref(path);
    }

    _subscribeTest(roomId) {
        let path = '/chat_messages/' + roomId;
        console.log("pathhh" + path);
        return firebase.database()
            .ref(path)
            .orderByKey()
            .startAt(this.lastMessageId)
            .on('child_added', function (snapshoot){
                    var item = snapshoot.val();
                    item.key = snapshoot.key;
                    this.lastMessageId = snapshoot.key;
                    this.messages.push(item);
                    this.setState ( {
                        isRefreshing: true});

                return item;
            }.bind(this));
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
    }

    componentWillUnmount() {
        this.unsubscribeFromDatabase();
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    keyboardDidShow(e) {
        //this.scrollView.scrollToEnd({ animated: true });
    }

    keyboardDidHide(e) {
        //this.scrollView.scrollToEnd({ animated: true });
    }

    componentDidMount() {
        this.setState({
            isRefreshing: false
        });
        this.scrollView.scrollToEnd({ animated: true });
        setTimeout(function () {
            this.scrollView.scrollToEnd({ animated: true });
        }.bind(this))
    }
}



