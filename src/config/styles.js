/**
 * Styles files which contains all styles that will be used in the app
 * Provides a way to centralize all styles and make them easily change-able by staying 
 * consisten across all the app
 */
import { StyleSheet, Dimensions } from 'react-native';
const window = Dimensions.get('window');

export const defaultStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        width: window.width,
        height: window.height
    },
    containerHorizontal: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        width: window.width,
        height: window.height
    },
    input: {
        height: 40,
        backgroundColor: '#fff',
        marginHorizontal: 10,
        marginVertical: 5,
        // paddingVertical: 5,
        // paddingHorizontal: 15,
        width: window.width - 30,
    },
    button: {
        height: 40,
        backgroundColor: '#fff',
        marginHorizontal: 10,
        marginVertical: 5,
        width: window.width - 10,
        paddingVertical: 5,
        paddingHorizontal: 15,
    }
});

export const styles = StyleSheet.create({

    //ChatView
  
    outer: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'space-between',
      backgroundColor: 'white'
    },
  
    messages: {
      flex: 1
    },
  
    //InputBar
  
    inputBar: {
      height:40,
      flexDirection: 'row',
      justifyContent: 'space-between',
      borderRadius:25,
      paddingHorizontal: 5,
      paddingVertical: 3,
      
    },
  
    textBox: {
      borderRadius: 5,
      borderWidth: 1,
      borderColor: 'gray',
      flex: 1,
      fontSize: 16,
      paddingHorizontal: 10
    },
  
    sendButton: {
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 5,
      marginRight: 5,
      paddingLeft:5,
      paddingRight:5,
      borderRadius: 25,
      backgroundColor: '#00897b'
    },

    fileButton: {
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 5,
      marginRight: 5,
      paddingLeft:5,
      paddingRight:5,
      borderRadius: 25,
      backgroundColor: '#3ca2e0'
    },
    loginButton: {
      marginLeft: 10,
      borderRadius: 25,
      backgroundColor: '#3ca2e0'
    },

    //MessageBubble
  
    messageBubble: {
        borderRadius: 5,
        marginTop: 8,
        marginRight: 10,
        marginLeft: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection:'row',
        flex: 1
    },
  
    messageBubbleLeft: {
      backgroundColor: '#DCDCDC',
      borderRadius:25
    },
  
    messageBubbleTextLeft: {
      color: 'black'
    },
  
    messageBubbleRight: {
      backgroundColor: '#00897b',
      borderRadius:25
    },
  
    messageBubbleTextRight: {
      color: 'white'
    },
  })