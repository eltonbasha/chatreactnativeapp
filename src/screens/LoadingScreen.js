import React from 'react';
import { View,ActivityIndicator,StatusBar,AppState } from 'react-native';
import * as Styles from '../config/styles';
import * as firebase from 'firebase';
import * as Utils from '../Utils';


export class LoadingScreen extends React.Component {
    constructor(props) {
      super(props);
    }
  
    checkIfLoggedIn(){
        const userName = Utils.readUsername()
        .then((userName) => {
            if(userName !== null){
                alert(" Welcome: " + userName);
                this.navigateTo('App')
            } else {
                this.navigateTo('Auth')
            }
        }).catch((error) => {
          console.log(error);
        });
    }

    navigateTo(where){ 
      this.props.navigation.navigate(where);
    }
    
    // Render any loading content that you like here
    render() {
      return (
        <View style = {Styles.defaultStyles.container}>
          <ActivityIndicator />
          <StatusBar barStyle="default" />
        </View>
      );
    }
    
    componentDidMount(){
        this.checkIfLoggedIn();
    }
  }