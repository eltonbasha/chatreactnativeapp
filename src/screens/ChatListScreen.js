import React from 'react';
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView, FlatList } from 'react-native';
import { FormLabel, FormInput, Input, Button, ListItem } from 'react-native-elements'
import * as Styles from '../config/styles';
import * as Utils from '../Utils';
import { TabNavigator } from 'react-navigation';
import * as firebase from 'firebase';
import { TouchableListItem } from '../utils/UtilComponents';


export class ChatListScreen extends React.Component {
    
    constructor(props) {
        super(props);
        this.rooms = []
        this.state = {
            isRefreshing: false
        }
        this.handleClick = this.handleClick.bind(this);
        this.subscribeToDatabase();
    }

    subscribeToDatabase() {
        let userId = Utils.readUser()
            .then((userId) => {
                return firebase.database().ref('/users/' + userId + '/rooms')
                    .on('value', this.addNewData())
            })
            .catch((error) => {
                console.log(error);
            });
    }

    unsubscribeFromDatabase() {
        let userId = Utils.readUser()
        .then((userId) => {
            firebase.database().ref('/users/' + userId + '/rooms').off();
        })
        .catch((error) => {
            console.log(error);
        })
    }

    render() {
        return (
            <View>
                <FlatList
                    data={this.rooms}
                    extraData={this.state.isRefreshin}
                    renderItem={this._renderRoom()}
                    refreshing={this.state.isRefreshin}
                    keyExtractor={(i) => i.id} />
            </View>
        );
    };

    componentWillUnmount() {
        console.log("Unsubscribing from database");
        this.unsubscribeFromDatabase();
    }

    _renderRoom() {
        return (room) => {
            console.log("Rooms ne render item : " +room.item.key);
            return (<TouchableListItem
                uname = {room.item.username}
                id = {room.item.key}
                title={room.item.username}
                key={room.item.key}
                onPressItem={this.handleClick} />
            )
        };
    }

    componentDidMount() {
        this.setState({
            isRefreshing: false
        });
    }

    addNewData() {
        return (snapshot) => {
            this.rooms = Utils.snapshotToArray(snapshot);
            this.setState({
                isRefreshin: true
            })
        }
    }

    handleClick(id, uname) {
        this.props.navigation.navigate('ChatRoom', { userId: id, username: uname });
        console.log("Id ne click: " + id);
    }
}