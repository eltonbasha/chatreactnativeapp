import * as firebase from 'firebase';
import * as Utils from '../Utils';
const md5 = require('blueimp-md5');

/************************REGISTER FUNCTIONS *****************************************/

export function updateUserStatus(userId,username) {
    let firebaseDb = firebase.database();
    let userStatusPath = "/user_status/" + userId + "/";
    let userChatPath = "/user_chat/" + userId + "/";
    let newUser = {}

    newUser[userStatusPath] = {
        status: true,
        username: username
    }
    newUser[userChatPath] = {
        chat_id: true
    }
    return firebaseDb.ref()
        .update(newUser)
        .catch((error) => {
            console.log(error);
        })
        .then(() => {
            return Utils.saveUser({
                id: userId,
                username : username
            });
        });
}

export function checkUsernameExists(username) {
    let firebaseDb = firebase.database();
    return firebaseDb
        .ref('users')
        .orderByChild('username')
        .equalTo(username)
        .once('value')
        .then((snapshot) => {
            console.log("Username exist : " + snapshot.exists());
            if (snapshot.exists()) {
                return true;
            } else {
                return false;
            }
        })
        .catch((error => {
            alert(error.message);
        }));
}

export function registerUser(user) {
    let firebaseDb = firebase.database();
    let newRef = firebaseDb.ref("/users")
        .push();
    return newRef.set({
        email: user.email,
        name: user.name,
        password: md5(user.password),
        surname: user.surname,
        username: user.username
    }).then(() => {
        return newRef;
    });
}


/************************LOGIN FUNCTIONS*****************************************/

/****
 * It's the same function as above but it needs to return the snapshoot value in logins case 
 * to check for password equality
 */
export function checkUsernameExistsAndGetValue(username) {
    let firebaseDb = firebase.database();
    return firebaseDb.ref('users')
        .orderByChild('username')
        .equalTo(username)
        .once('value')
        .catch((error => {
            alert(error.message);
        }));
}


export function changeStatus(userId) {
    let firebaseDb = firebase.database();
    return firebaseDb
        .ref('/user_status/' + userId + '/status')
        .set(true)
        .catch((error) => {
            console.log(error);
        });
}