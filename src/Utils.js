import { AsyncStorage } from 'react-native';


export const saveUser = async function(user){ //This needs async as this function is async
    console.log("User ne save" + JSON.stringify(user));
    try{
        let userId = user.id;
        let username = user.username;
        return await AsyncStorage.multiSet([["user_id",userId],["username",username]]);
    } catch (error){
        console.log(error);
    }
}

export const readUser = async function(){
    try {
        const userId = await AsyncStorage.getItem("user_id")
        .then((userId) => {
            return userId;
        });
        return userId;
    } catch(error){
        console.log(error);
    }
}

export const readUsername = async function(){
    try {
        const username = await AsyncStorage.getItem("username")
        .then((username) => {
            return username;
        });
        return username;
    } catch(error){
        console.log(error);
    }
}

export const clearUser = async function(){
    try{
        return await AsyncStorage.multiRemove(["user_id","username"]);
    } catch(error) {
        console.log(error);
    }
}

export const snapshotToArray = function(snapshot){
        var returnArr = [];
    
        snapshot.forEach(function(childSnapshot) {
            var item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });
    
        return returnArr;
}
