import { FormLabel, FormInput, Input, Button } from 'react-native-elements'
import React from 'react';
import { View, KeyboardAvoidingView } from 'react-native';
import * as Config from '../config/config';
import * as firebase from 'firebase';
import * as Styles from '../config/styles';
import * as Utils from '../Utils';
const md5 = require('blueimp-md5');
import * as FirebaseDb from '../data/AuthDBAccess'

export class LoginScreen extends React.Component {


    constructor(props) {
        super(props);

        let loginField = function () {
            this._value = null;
            this.set = function (v) {
                this._value = v;
            };
            this.value = function () {
                return this._value;
            };
            this.length = function (){
                return this._value ? this._value.length :0;
            }
        };

        this.state = {
            username: new loginField(),
            password: new loginField()
        }
    }

    render() {
        return (
            <KeyboardAvoidingView style={Styles.defaultStyles.container} behavior='padding' >
                <FormLabel>Username</FormLabel>
                <FormInput
                    onChangeText={this.checkLength(this.state.username)} />

                <FormLabel>Password</FormLabel>
                <FormInput
                    secureTextEntry={true}
                    onChangeText={this.checkLength(this.state.password)} />

                <View style={Styles.defaultStyles.containerHorizontal}>
                    <Button
                        containerViewStyle={{ marginLeft: -10, marginRight: null, width: "100%" }}
                        buttonStyle={Styles.styles.loginButton}
                        large
                        raised
                        title='Login'
                        onPress={() => { this.loginClick() }} />

                </View>
            </KeyboardAvoidingView>
        );
    }


    checkFieldsLength() {
        let checkFields = Object.values(this.state)
            .every((text) => {
                console.log(text)
                return text.length() != 0
            });
        return checkFields;
    }

    loginClick() {
        this.checkFieldsLength() ?
            FirebaseDb.checkUsernameExistsAndGetValue(this.state.username.value())
                .then((snapshot) => {
                    snapshot.val() ? this.checkPassword(snapshot) : alert("This username doesn't exist");
                }) :
            alert("Please fill all the fields")
    }


    checkPassword(snapshoot) {
        let users = Utils.snapshotToArray(snapshoot);
        console.log(JSON.stringify(snapshoot.val()));
        let user;
        let isSame = users.every((value) => {
            let test = value ?
                this.checkMD5Equality(value.password, this.state.password.value(), value.key)
                : null;
            user = value;
            return (test ? true : false);
        })

        isSame ?
            this.saveUser(user.key, user.username)
                .then(() => {
                    this.props.navigation.navigate('AuthLoading');
                }) :
            alert('Wrong password');
    }

    saveUser(userId, username) {
        let user = {
            id: userId,
            username: username
        }
        return FirebaseDb.changeStatus(userId)
            .then(() => {
                return Utils.saveUser(user);
            });
    }

    checkMD5Equality(password, inputPassword, userId) {
        console.log(password + '    vs    ' + md5(inputPassword));
        return (password == md5(inputPassword) ? userId : null);
    }

    checkLength(field) {
        return function (input) {
            console.log('Texti' + input);
            input.length > 3 ? field.set(input) : field.set(null);
        }
    }
}